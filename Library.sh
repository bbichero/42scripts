#!/bin/bash

GOINFRE_PATH="/goinfre/bbichero"
SGOINFRE_PATH="/goinfre/sgoinfre/Perso/bbichero"
LIBRARY_PATH="/Users/bbichero/Library"

function	init_goinfre() {
	mkdir -v "$GOINFRE_PATH/Library"
}

# Check if Library folder is a directory
#if [[ -L "$LIBRARY_PATH" && -d "$LIBRARY_PATH" ]]
#then
#	echo "Library symbolic already exist"
#	exit 1
#fi
#
#if [ -d $LIBRARY_PATH ]
# Check if Goinfre path exist
if [ -d $GOINFRE_PATH ]; then
	mkdir -v $GOINFRE_PATH
fi

# Check if SGoinfre path exist
if [ -d $SGOINFRE_PATH ]; then
	mkdir -v $SGOINFRE_PATH
fi

# Check if Goinfre Library path exist
if [ ! -d "$GOINFRE_PATH/Libraryi" ]; then

	# Initialise Library in /goinfre
	echo "Do you wish to migrate your Library directory to /goinfre ?"
	select yn in "Yes" "No"; do
		case $yn in
			Yes ) init_goinfre ; break;;
			No ) exit;;
		esac
	done

fi
